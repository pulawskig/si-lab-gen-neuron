import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class NeuralNetwork {

	private static final double alpha = 0.0001;
	private static final int networkSize = 1;
	private static final int epochs = 1000;
	
	private double sigma = 0.1d;
	
	private ArrayList<Neuron> neurons = new ArrayList<Neuron>();

	private class Neuron {
		private Array2DRowRealMatrix theta;
		private double alpha;
		
		public Neuron(int dim, double alpha) {
			double th[] = new double[dim];
			Random r = new Random();
			for(int i = 0; i < dim; i++) {
				th[i] = r.nextDouble() * 2D - 1D;
			}
			this.theta = new Array2DRowRealMatrix(th);
			this.alpha = alpha;
		}
		
		public Neuron(Array2DRowRealMatrix theta, double alpha) {
			this.theta = theta;
			this.alpha = alpha;
		}
		
		public void teach(Array2DRowRealMatrix X) {
			//theta = (Array2DRowRealMatrix) theta.add(X.subtract(theta).scalarMultiply(alpha));
			Array2DRowRealMatrix y = theta.add(new Array2DRowRealMatrix((new NormalDistribution(0d, sigma)).sample(3)));
			
		}
		
		public double feed(Array2DRowRealMatrix X) {
			return theta.transpose().multiply(X).getEntry(0, 0);
		}
	}
	
	public NeuralNetwork(int n, int dim, double alpha) {
		for(int i = 0; i < n; i++) {
			neurons.add(new Neuron(dim, alpha));
		}
	}
	
	public void teach(List<Array2DRowRealMatrix> dataset, int epochs) {
		/*for(int i = 0; i < epochs; i++) {
			double max = -100D;
			int maxj = -1;
			for(Array2DRowRealMatrix set : dataset) {
				for(int j = 0; j < neurons.size(); j++) {
					double a = neurons.get(j).feed(set);
					if(a > max) {
						max = a;
						maxj = j;
					}
				}
				
				neurons.get(maxj).teach(set);
			}
		} */
		
		for(int i = 0; i < epochs; i++) {
			
		}
	}
	
	public ArrayList<Integer> test(List<Array2DRowRealMatrix> testset) {
		ArrayList<Integer> ret = new ArrayList<>();
		double max = Double.MIN_VALUE;
		int maxj = -1;
		for(Array2DRowRealMatrix set : testset) {
			for(int j = 0; j < neurons.size(); j++) {
				double a = neurons.get(j).feed(set);
				if(a > max) {
					max = a;
					maxj = j;
				}
			}
			ret.add(maxj);
		}
		return ret;
	}
	
	public static ArrayList<Array2DRowRealMatrix> getDataset(String path, char delimiter) {
		ArrayList<Array2DRowRealMatrix> dataset = new ArrayList<>();
		try {
			CSVParser parser = CSVParser.parse(new File(path), Charset.forName("UTF-8"), CSVFormat.DEFAULT.withDelimiter(delimiter));
			List<CSVRecord> records = parser.getRecords();
			for(CSVRecord record : records) {
				double array[] = new double[record.size() - 1];
				for(int i = 0; i < record.size() - 1; i++)
					array[i] = Double.parseDouble(record.get(i));
				dataset.add(new Array2DRowRealMatrix(array));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dataset;
	}
	
	public static ArrayList<Double> getLabels(String path, char delimiter) {
		ArrayList<Double> labels = new ArrayList<>();
		try {
			CSVParser parser = CSVParser.parse(new File(path), Charset.forName("UTF-8"), CSVFormat.DEFAULT.withDelimiter(delimiter));
			List<CSVRecord> records = parser.getRecords();
			for(CSVRecord record : records) {
				labels.add(Double.parseDouble(record.get(record.size() - 1)));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return labels;
	}
	
	public static ArrayList<Array2DRowRealMatrix> normalize(ArrayList<Array2DRowRealMatrix> input) {
		double max[] = new double[input.size()];
		double min[] = new double[input.size()];
		for(int i = 0; i < input.size(); i++) {
			max[i] = Double.MIN_VALUE;
			min[i] = Double.MAX_VALUE;
		}
		
		for(Array2DRowRealMatrix matrix : input) {
			for(int i = 0; i < matrix.getRowDimension(); i++) {
				if(matrix.getEntry(i, 0) > max[i])
					max[i] = matrix.getEntry(i, 0);
				else if(matrix.getEntry(i, 0) < min[i])
					min[i] = matrix.getEntry(i, 0);
			}
		}
		
		for(Array2DRowRealMatrix matrix : input) {
			for(int i = 0; i < matrix.getRowDimension(); i++) {
				matrix.setEntry(i, 0, (matrix.getEntry(i, 0) - min[i]) / (max[i] - min[i]));
			}
		}
		
		return input;
	}
	
	public static void main(String[] args) {
		NeuralNetwork nn = new NeuralNetwork(networkSize, 3, alpha);
		
		ArrayList<Array2DRowRealMatrix> learnset = getDataset("water.learn.txt.csv", ' ');
		ArrayList<Double> learnlabels = getLabels("water.learn.txt.csv", ' ');
		ArrayList<Array2DRowRealMatrix> testset = getDataset("water.test.txt.csv", ' ');
		ArrayList<Double> testlabels = getLabels("water.test.txt.csv", ' ');
		
		
		
		/*XYSeriesCollection collection = new XYSeriesCollection();
		ArrayList<XYSeries> seriesList = new ArrayList<>();
		
		ArrayList<Array2DRowRealMatrix> fullset = getDataset();
		ArrayList<Array2DRowRealMatrix> normalized = getDataset();
		normalize(normalized);
		
		ArrayList<Array2DRowRealMatrix> datasetNormalized = new ArrayList<>();
		ArrayList<Array2DRowRealMatrix> testsetNormalized = new ArrayList<>();
		ArrayList<Array2DRowRealMatrix> testset = new ArrayList<>();
		for(int i = 0; i < fullset.size(); i++) {
			if(i % 10 == 5) {
				testsetNormalized.add(normalized.get(i));
				testset.add(fullset.get(i));
			} else {
				datasetNormalized.add(normalized.get(i));
			}
		}
		
		nn.teach(datasetNormalized, epochs);
		ArrayList<Integer> output = nn.test(testsetNormalized);
		
		TreeMap<Integer, Integer> map = new TreeMap<>();
		int groups = 0;
		
		for(Integer i : output) {
			if(!map.containsKey(i)) {
				map.put(i, groups);
				seriesList.add(new XYSeries("Grupa " + (groups + 1)));
				groups++;
			}
		}
		
		for(int i = 0; i < output.size(); i++) {
			seriesList.get(map.get(output.get(i))).add(testset.get(i).getEntry(2, 0), testset.get(i).getEntry(3, 0));
			System.out.println(testset.get(i) + ": Grupa " + (map.get(output.get(i)) + 1));
		}
		
		
		for(XYSeries s : seriesList) {
			collection.addSeries(s);
		}
		
		JFreeChart chart = ChartFactory.createScatterPlot("", "petal length in cm", "petal width in cm", collection, PlotOrientation.VERTICAL, true, true, false);
		ChartFrame frame = new ChartFrame("", chart);
		frame.pack();
		frame.setVisible(true);*/
	}

}